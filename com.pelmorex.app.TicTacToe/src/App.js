import { Lightning, Utils } from '@lightningjs/sdk'
import Splash from './Splash.js'
import Main from './Main.js'
import Game from './Game.js'

export default class App extends Lightning.Component {
  static getFonts() {
    return [
      // { family: 'pixel', url: Utils.asset('fonts/pixel.ttf'), descriptor: {} }
      { family: 'Mexican', url: Utils.asset('fonts/MexicanTequila.ttf'), descriptor: {} },
    ]
  }

  static _template() {
    return {
      Background: {
        w: 1920,
        h: 1080,
        // color: 0xfffbb03b,
        // color: 0xff000000,
        src: Utils.asset('images/background.png'),
      },
      TestBox: {
        w: 700,
        h: 700,
        x: 100,
        y: 100,
        SomeText: {
          x: 200,
          y: 140,
          text: 'COLOR DOTS',
          fontFace: 'Mexican',
          fontSize: 64,
          textColor: 0xff0000ff,
        },
        // color: 0xfffbb03b,
        // color: 0xff000000,
        src: Utils.asset('images/back_01.png'),
        // TestBox2: {
        //   w: 70,
        //   h: 70,
        //   x: 10,
        //   y: 10,
        //   zIndex: 2,
        //   // color: 0xfffbb03b,
        //   // color: 0xff000000,
        //   src: Utils.asset('images/back_02.png'),
        //   // shader: { type: lng.shaders.Inversion }
        // },
      },
      Logo: {
        x: 0,
        y: 140,
        text: { text: 'TicTacToe', fontFace: 'Mexican' },
      },
      Logo2: {
        x: 300,
        y: 300,
        text: {
          text: 'Por que no',
          fontFace: 'Mexican',
          fontSize: 64,
          textColor: 0x7f00ff00,
        },
      },
      Text: {
        mount: 0.5,
        x: 960,
        y: 720,
        text: {
          text: "Let's start Building!",
          fontFace: 'Regular',
          fontSize: 64,
          textColor: 0xbbffffff,
          highlight: false,
          highlightColor: 0xff00ff00,
          shadow: true,
          shadowColor: 0xbbff0000,
          shadowOffsetX: 2,
          shadowOffsetY: 2,
          shadowBlur: 2,
        },
      },

      // rect: true, color: 0xff000000,
      Splash: {
        type: Splash,
        signals: { loaded: true },
        alpha: 0,
      },
      Main: {
        type: Main,
        alpha: 0,
        signals: { select: 'menuSelect' },
      },
      Game: {
        type: Game,
        alpha: 0,
      },
      RectangleDefault: {
        x: 100,
        y: 100,
        w: 200,
        h: 100,
        rect: true,
      },
      RectangleWithColor: {
        x: 400,
        y: 100,
        w: 200,
        h: 100,
        rect: true,
        color: 0xff1c27bc,
      },
      RectangleWithGradientTopBottom: {
        x: 100,
        y: 300,
        w: 200,
        h: 100,
        rect: true,
        colorTop: 0xff636efb,
        colorBottom: 0xff1c27bc,
      },
      RectangleWithGradientLeftRight: {
        x: 400,
        y: 300,
        w: 200,
        h: 100,
        rect: true,
        colorLeft: 0xff636efb,
        colorRight: 0xff1c27bc,
      },
      RectangleWithGradientDiagonal: {
        x: 100,
        y: 500,
        w: 200,
        h: 100,
        rect: true,
        colorUl: 0xff636efb,
        colorUr: 0xff00ff00,
        colorBr: 0xff1c27bc,
        colorBl: 0xff00ff00,
      },
      RectangleWithGradientDiagonalMixed: {
        x: 400,
        y: 500,
        w: 200,
        h: 100,
        rect: true,
        colorLeft: 0xff00ff00,
        colorBr: 0xff1c27bc,
        colorUr: 0xffff0000,
      },
    }
  }

  _init() {
    this.tag('Logo2').patch({
      text: {
        text: 'This is new',
        textColor: 0xff00ff00,
      },
    })

    this.tag('Logo2').setSmooth('x', 10, { duration: 1 })

    // this.tag('TestBox').patch({
    //   TestBox2: {
    //     src: Utils.asset('images/back_01.png'),
    //   }
    // })

    // this.tag('TestBox').on('txLoaded', () => {
    //   console.log('xxx texture loaded: ' + this.tag('TestBox').src)
    //   this.tag('TestBox').patch({
    //     TestBox2: {
    //       src: Utils.asset('images/back_01.png'),
    //     }
    //   })
    // })
  }

  _setup() {
    this._setState('Splash')
  }

  static _states() {
    return [
      class Splash extends this {
        $enter() {
          this.tag('Splash').setSmooth('alpha', 1)
        }

        $exit() {
          this.tag('Splash').setSmooth('alpha', 0)
        }

        loaded() {
          this._setState('Main')
        }
      },
      class Main extends this {
        $enter() {
          this.tag('Main').patch({
            smooth: { alpha: 1, y: 0 },
          })
        }

        $exit() {
          this.tag('Main').patch({
            smooth: { alpha: 0, y: 100 },
          })
        }

        menuSelect({ item }) {
          if (this._hasMethod(item.action)) {
            return this[item.action]()
          }
        }

        start() {
          this._setState('Game')
        }

        // change focus path to main
        // component which handles the remotecontrol
        _getFocused() {
          return this.tag('Main')
        }
      },
      class Game extends this {
        $enter() {
          this.tag('Game').setSmooth('alpha', 1)
        }

        $exit() {
          this.tag('Game').setSmooth('alpha', 0)
        }

        _getFocused() {
          return this.tag('Game')
        }
      },
    ]
  }
}
